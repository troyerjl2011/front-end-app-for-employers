import { push } from "connected-react-router";

export const selectWorkout = function(workoutName) {
    return {type: "SELECT_WORKOUT", payload: workoutName }
}
export const completedExercise = function(todoIdToToggle) {
    return {type: "COMPLETED_EXERCISE", payload: todoIdToToggle }
}

export const resetExercise = resetWorkouts => (dispatch) => {
    dispatch(push("/profile"));

    dispatch({type: "RESET_EXERCISE_PAGE", payload: resetWorkouts });
}