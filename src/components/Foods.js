const Foods = {
  total_hits: 16052,
  max_score: 12.04918,
  hits: [
    {
      _index: "f762ef22-e660-434f-9071-a10ea6691c27",
      _type: "item",
      _id: "513fceb475b8dbbc21000f93",
      _score: 12.04918,
      fields: {
        item_id: "513fceb475b8dbbc21000f93",
        item_name: 'Apple - 1 large (3-1/4" dia)',
        brand_name: "USDA",
        nf_calories: 115.96,
        nf_total_carbohydrate: 30.8,
        nf_serving_size_qty: 1,
        nf_serving_size_unit: "serving"
      }
    },
    {
      _index: "f762ef22-e660-434f-9071-a10ea6691c27",
      _type: "item",
      _id: "513fceb475b8dbbc21000f92",
      _score: 11.961595,
      fields: {
        item_id: "513fceb475b8dbbc21000f92",
        item_name: 'Apple - 1 medium (3" dia)',
        brand_name: "USDA",
        nf_calories: 94.64,
        nf_total_carbohydrate: 25.13,
        nf_serving_size_qty: 1,
        nf_serving_size_unit: "serving"
      }
    },
    {
      _index: "f762ef22-e660-434f-9071-a10ea6691c27",
      _type: "item",
      _id: "513fceb475b8dbbc21000f95",
      _score: 11.879342,
      fields: {
        item_id: "513fceb475b8dbbc21000f95",
        item_name: "Apple - 1 cup, quartered or chopped",
        brand_name: "USDA",
        nf_calories: 65,
        nf_total_carbohydrate: 17.26,
        nf_serving_size_qty: 1,
        nf_serving_size_unit: "serving"
      }
    },
    {
      _index: "f762ef22-e660-434f-9071-a10ea6691c27",
      _type: "item",
      _id: "5d766065c6486a815b437198",
      _score: 11.86028,
      fields: {
        item_id: "5d766065c6486a815b437198",
        item_name: "Apple - 1 oz",
        brand_name: "USDA",
        nf_calories: 14.74,
        nf_total_carbohydrate: 3.91,
        nf_serving_size_qty: 1,
        nf_serving_size_unit: "serving"
      }
    },
    {
      _index: "f762ef22-e660-434f-9071-a10ea6691c27",
      _type: "item",
      _id: "513fceb475b8dbbc21000f94",
      _score: 11.824786,
      fields: {
        item_id: "513fceb475b8dbbc21000f94",
        item_name: "Apple - 1 cup slices",
        brand_name: "USDA",
        nf_calories: 56.68,
        nf_total_carbohydrate: 15.05,
        nf_serving_size_qty: 1,
        nf_serving_size_unit: "serving"
      }
    },
    {
      _index: "f762ef22-e660-434f-9071-a10ea6691c27",
      _type: "item",
      _id: "513fceb475b8dbbc21000f90",
      _score: 11.804714,
      fields: {
        item_id: "513fceb475b8dbbc21000f90",
        item_name: 'Apple - 1 extra small (2-1/2" dia)',
        brand_name: "USDA",
        nf_calories: 52.52,
        nf_total_carbohydrate: 13.95,
        nf_serving_size_qty: 1,
        nf_serving_size_unit: "serving"
      }
    },
    {
      _index: "f762ef22-e660-434f-9071-a10ea6691c27",
      _type: "item",
      _id: "513fceb475b8dbbc21000f98",
      _score: 11.78451,
      fields: {
        item_id: "513fceb475b8dbbc21000f98",
        item_name: 'Apples, raw, without skin - 1 large (3-1/4" dia)',
        brand_name: "USDA",
        nf_calories: 103.68,
        nf_total_carbohydrate: 27.56,
        nf_serving_size_qty: 1,
        nf_serving_size_unit: "serving"
      }
    },
    {
      _index: "f762ef22-e660-434f-9071-a10ea6691c27",
      _type: "item",
      _id: "513fceb475b8dbbc21000f8f",
      _score: 11.761192,
      fields: {
        item_id: "513fceb475b8dbbc21000f8f",
        item_name: "Apple - 1 NLEA serving",
        brand_name: "USDA",
        nf_calories: 125.84,
        nf_total_carbohydrate: 33.42,
        nf_serving_size_qty: 1,
        nf_serving_size_unit: "serving"
      }
    },
    {
      _index: "f762ef22-e660-434f-9071-a10ea6691c27",
      _type: "item",
      _id: "513fceb475b8dbbc21000f97",
      _score: 11.652182,
      fields: {
        item_id: "513fceb475b8dbbc21000f97",
        item_name: 'Apples, raw, without skin - 1 medium (3" dia)',
        brand_name: "USDA",
        nf_calories: 77.28,
        nf_total_carbohydrate: 20.54,
        nf_serving_size_qty: 1,
        nf_serving_size_unit: "serving"
      }
    },
    {
      _index: "f762ef22-e660-434f-9071-a10ea6691c27",
      _type: "item",
      _id: "513fceb475b8dbbc21000f91",
      _score: 11.607929,
      fields: {
        item_id: "513fceb475b8dbbc21000f91",
        item_name: 'Apple - 1 small (2-3/4" dia)',
        brand_name: "USDA",
        nf_calories: 77.48,
        nf_total_carbohydrate: 20.58,
        nf_serving_size_qty: 1,
        nf_serving_size_unit: "serving"
      }
    },
    {
      _index: "f762ef22-e660-434f-9071-a10ea6691c27",
      _type: "item",
      _id: "513fceb475b8dbbc21000f99",
      _score: 11.435527,
      fields: {
        item_id: "513fceb475b8dbbc21000f99",
        item_name: "Apples, raw, without skin - 1 cup slices",
        brand_name: "USDA",
        nf_calories: 52.8,
        nf_total_carbohydrate: 14.04,
        nf_serving_size_qty: 1,
        nf_serving_size_unit: "serving"
      }
    },
    {
      _index: "f762ef22-e660-434f-9071-a10ea6691c27",
      _type: "item",
      _id: "513fceb475b8dbbc21000f96",
      _score: 11.328346,
      fields: {
        item_id: "513fceb475b8dbbc21000f96",
        item_name: 'Apples, raw, without skin - 1 small (2-3/4" dia)',
        brand_name: "USDA",
        nf_calories: 63.36,
        nf_total_carbohydrate: 16.84,
        nf_serving_size_qty: 1,
        nf_serving_size_unit: "serving"
      }
    },
    {
      _index: "f762ef22-e660-434f-9071-a10ea6691c27",
      _type: "item",
      _id: "5a7aa4861ae3b0050df80923",
      _score: 3.4695635,
      fields: {
        item_id: "5a7aa4861ae3b0050df80923",
        item_name: "Apple",
        brand_name: "Georgia Apple Co",
        nf_calories: 65,
        nf_total_carbohydrate: 17,
        nf_serving_size_qty: 1,
        nf_serving_size_unit: "serving"
      }
    },
    {
      _index: "f762ef22-e660-434f-9071-a10ea6691c27",
      _type: "item",
      _id: "552ff10584ef2600245b6e69",
      _score: 3.4631991,
      fields: {
        item_id: "552ff10584ef2600245b6e69",
        item_name: "Apple",
        brand_name: "Collatio",
        nf_calories: 70,
        nf_total_carbohydrate: 19,
        nf_serving_size_qty: 1,
        nf_serving_size_unit: "serving"
      }
    },
    {
      _index: "f762ef22-e660-434f-9071-a10ea6691c27",
      _type: "item",
      _id: "5bebc8e6b0b4c7b7311fbd63",
      _score: 3.4631991,
      fields: {
        item_id: "5bebc8e6b0b4c7b7311fbd63",
        item_name: "Apple",
        brand_name: "Unknown",
        nf_calories: 80,
        nf_total_carbohydrate: 22,
        nf_serving_size_qty: 1,
        nf_serving_size_unit: "serving"
      }
    },
    {
      _index: "f762ef22-e660-434f-9071-a10ea6691c27",
      _type: "item",
      _id: "5a225145833191dc5a882f1a",
      _score: 3.4631991,
      fields: {
        item_id: "5a225145833191dc5a882f1a",
        item_name: "Apple",
        brand_name: "Unknown",
        nf_calories: 130,
        nf_total_carbohydrate: 34,
        nf_serving_size_qty: 1,
        nf_serving_size_unit: "serving"
      }
    },
    {
      _index: "f762ef22-e660-434f-9071-a10ea6691c27",
      _type: "item",
      _id: "5c67be807cada5ff03973cf3",
      _score: 3.4631991,
      fields: {
        item_id: "5c67be807cada5ff03973cf3",
        item_name: "Apple",
        brand_name: "Gerber",
        nf_calories: 35,
        nf_total_carbohydrate: 7,
        nf_serving_size_qty: 1,
        nf_serving_size_unit: "serving"
      }
    },
    {
      _index: "f762ef22-e660-434f-9071-a10ea6691c27",
      _type: "item",
      _id: "5c0b72f2686bb38f22b42068",
      _score: 3.4631991,
      fields: {
        item_id: "5c0b72f2686bb38f22b42068",
        item_name: "Apple",
        brand_name: "Ambrosia",
        nf_calories: 130,
        nf_total_carbohydrate: 34,
        nf_serving_size_qty: 1,
        nf_serving_size_unit: "serving"
      }
    },
    {
      _index: "f762ef22-e660-434f-9071-a10ea6691c27",
      _type: "item",
      _id: "574897ed399776f36c85d36b",
      _score: 3.4229355,
      fields: {
        item_id: "574897ed399776f36c85d36b",
        item_name: "Apple",
        brand_name: "ASDA",
        nf_calories: 43.23,
        nf_total_carbohydrate: 9.3,
        nf_serving_size_qty: 1,
        nf_serving_size_unit: "serving"
      }
    },
    {
      _index: "f762ef22-e660-434f-9071-a10ea6691c27",
      _type: "item",
      _id: "59e0689281df9d0b069740c6",
      _score: 3.4102452,
      fields: {
        item_id: "59e0689281df9d0b069740c6",
        item_name: "Apple",
        brand_name: "Apple Barn Orchard and Winery",
        nf_calories: 130,
        nf_total_carbohydrate: 34,
        nf_serving_size_qty: 1,
        nf_serving_size_unit: "serving"
      }
    }
  ]
};
