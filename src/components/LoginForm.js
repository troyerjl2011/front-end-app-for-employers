import React, { Component } from "react";
import { connect } from "react-redux";
import { loginThenGoToUserProfile as login } from "../actions";
import Spinner from "react-spinkit";
import { Form, FormInput, FormGroup } from "shards-react";
import { Icon } from "semantic-ui-react";


// // element.style {
// //   background-color: lightgray;
// // };

// /* <div id="div1">
//   <p id="p1">Next level fitness is an app where you can accuractely log in calories, carbs, and protien,
// you will also be able to log in workouts. it will be displayed as charts.</p>
// </div> */

// const wrapper = {
//   display: 'flex',
//   flexDirection: 'column',
//   alignItems: 'center',
//   justifyContent: 'space-around',
//   width: "100%",
//   height: "500px",
 
//   color: '#444',
//   border: '1px solid #1890ff',
// };

// const wrapper2 = {
//   display: 'flex',
//   flexDirection: 'column',
//   alignItems: 'center',
//   justifyContent: 'center',
//   width: "100%",
//   height: "400px",
 
  
  
// };


class LoginForm extends Component {
  state = { username: "", password: "" };

  handleLogin = e => {
    e.preventDefault();
    this.props.login(this.state);
  };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    const { isLoading, err } = this.props;
    return (
      <div className="loginForm">
   
      
      
      <div style={{display:"flex",
      justifyContent: "center",
      height: "100%",
      alignItems: "center",
  }} >
    <div id="div1"> 
  <h2 id="p1">Next level fitness is an app where you can accuractely log in calories, carbs, and protien,
you will also be able to log in workouts. It will be displayed as charts.</h2></div>
      <Form className="signinForm">
        <h1>Next Level Fitness</h1>
        <form onSubmit={this.handleLogin}>
        <FormGroup>
        <label htmlFor="#username">Username</label>
        <FormInput 
          id="#username" 
          placeholder="Username" 
          autoFocus 
          name="username"
          required
          onChange={this.handleChange} />
        </FormGroup>
        <FormGroup>
          <label htmlFor="password">Password</label>
          <FormInput 
            type="password" 
            id="#password" 
            placeholder="Password" 
            name="password"
            required
            onChange={this.handleChange}
            />
        </FormGroup>
          <button type="submit" disabled={isLoading}>
            Login
          </button>
        </form>
        {isLoading && <Spinner name="circle" color="blue" />}
        {err && <p style={{ color: "red" }}>{err}</p>}
      </Form>
      <Icon name="heart" color="red" size="massive" />
      </div>
      </div>
    );
  }
}





export default connect(
  ({ auth }) => ({
    isLoading: auth.loginLoading,
    err: auth.loginError
  }),
  { login }
)(LoginForm);