import React, { Component } from "react";
import Dummy from "../data/dummy.json";

class FakeUsers extends Component {
  render() {
    return (
      <div>
        {Dummy.map((dummyDetail, index) => {
          return (
            <div>
              <h3>{dummyDetail.fullName}</h3>
              <p>{dummyDetail.age}</p>
              <p>{dummyDetail.height}</p>
              <p>{dummyDetail.weight}</p>
              <p>{dummyDetail.benchPress}</p>
              <p>{dummyDetail.squat}</p>
              <p>{dummyDetail.steps}</p>
              <p>{dummyDetail.caloriesBurned}</p>
              <p>{dummyDetail.caloriesConsumed}</p>
              <p>{dummyDetail.macronutrientRatio}</p>
            </div>
          );
        })}
      </div>
    );
  }
}

export default FakeUsers;
