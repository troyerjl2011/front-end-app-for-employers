import React, { Component } from "react";
import { connect } from "react-redux";
import { Grid } from "semantic-ui-react";
import { Button } from "shards-react";

class FoodResults extends Component {
  render() {
    return (
      <>
        {this.props.food.map(foodItem => {
          return (
            <>
              <Button className="foodButton" theme="primary" size="sm">Add Food to Your Profile</Button>

              <Grid divided centered celled="internally">
                <Grid.Row>
                  <Grid.Column>
                    {" "}
                    <h5>{foodItem.fields.item_name}</h5>{" "}
                  </Grid.Column>
                  <Grid.Column>
                    {" "}
                    <p>Caloies-{foodItem.fields.nf_calories}</p>{" "}
                  </Grid.Column>
                  <Grid.Column>
                    {" "}
                    <p>Fat-{foodItem.fields.nf_total_fat}</p>{" "}
                  </Grid.Column>
                  <Grid.Column>
                    {" "}
                    <p>Carbs-{foodItem.fields.nf_total_carbohydrate}</p>{" "}
                  </Grid.Column>
                  <Grid.Column>
                    {" "}
                    <p>Sugar-{foodItem.fields.nf_sugars}</p>{" "}
                  </Grid.Column>
                  <Grid.Column>
                    <p>Protien-{foodItem.fields.nf_protein}</p>{" "}
                  </Grid.Column>
                  <Grid.Column>
                    <p>
                      Serving Size-{foodItem.fields.nf_servings_per_container}
                    </p>{" "}
                  </Grid.Column>
                  <Grid.Column>
                    <p>
                      Serving Size Per Quantity-
                      {foodItem.fields.nf_serving_size_qty}
                    </p>
                  </Grid.Column>
                  <Grid.Column>
                    <p>
                      Serving Size Per Unit-
                      {foodItem.fields.nf_serving_size_unit}
                    </p>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </>
          );
        })}
      </>
    );
  }
}
const mapStateToProps = state => {
  return { food: state.nutrition.food };
};

export default connect(mapStateToProps)(FoodResults);
