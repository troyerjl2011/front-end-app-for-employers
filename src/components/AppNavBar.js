import React, { Component } from "react";
import { Link } from "react-router-dom";
import SearchBar from "./SearchBar";
import { Navbar, Nav, ButtonGroup, Button } from "shards-react";
import { Icon } from "semantic-ui-react";

class AppNavBar extends Component {
  render() {
    return (
      <>
        <Navbar theme="secondary">
          <div className="nav">
            <h1>Next Level Fitness</h1>
            <Icon name="heart" color="red" size="big" />
            <Nav>
              <ButtonGroup>
                <Link to="/profile">
                  <Button className="backToProfile" size="sm">
                    Go Back To Profile
                  </Button>
                </Link>
                <Link to="/workout">
                  <Button className="workoutButton" size="sm" theme="primary">
                    {" "}
                    Checkout your workout
                  </Button>
                </Link>
                <Link to="/stepsTaken">
                  <Button
                    className="stepsTakenButton"
                    size="sm"
                    theme="primary"
                  >
                    Add Steps Taken
                  </Button>
                </Link>
              </ButtonGroup>
              <SearchBar />
            </Nav>
          </div>
        </Navbar>
      </>
    );
  }
}

export default AppNavBar;
