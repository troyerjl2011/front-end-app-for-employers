import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Form, FormInput, FormGroup, Button } from "shards-react";

const wrapper = {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'space-around',
  width: "100%",
  height: "500px",
 
  color: '#444',
  border: '1px solid #1890ff',
};

const wrapper2 = {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'center',
  width: "250px",
  height: "100px",

};


class StepsTaken extends Component {
  
eventHandler = (event) => {
  event.preventDefault();
  
}  

  render() {
    return (
      <>
        <div style={wrapper}>
        <Form onSubmit={this.eventHandler}>
        <Link to="/profile">
          <Button squared style={wrapper2}>Go Back To Profile</Button>
        </Link>
        <FormGroup style={wrapper2}>
          <FormInput id="#stepsTaken" placeholder="Steps Taken" />
        </FormGroup>
        <Link to="/profile">
        <Button squared style={wrapper2}>Enter Steps Taken</Button>
        </Link>
      </Form>
      
      
      </div>
        
      </>
    );
  }
}

export default StepsTaken;