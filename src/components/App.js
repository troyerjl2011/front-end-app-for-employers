import React, { Component } from "react";
import {  Route } from "react-router-dom";
import { LoginForm, UserProfile, StepsTaken, Workout, Macronutrients, Calories, SearchBar, FakeUsers, AppNavBar } from ".";
import FoodResults from "./FoodResults";




class App extends Component {
  render() {
    return (
        <React.Fragment>
          
        <Route exact path={"/"} render={() => <LoginForm />} />
        <Route exact path={["/profile", "/stepsTaken", "/workout", "/Calories", "/Macronutrients", "/foodresults"]} render={() => <AppNavBar />} />
        <Route exact path={"/profile"} render={() => <UserProfile />} />
        <Route exact path={"/stepsTaken"} render={() => <StepsTaken />} />
        <Route exact path={"/workout" }render={() => <Workout />} />
        <Route exact path={"/Macronutrients"} render={() => <Macronutrients />} />
        <Route exact path={"/Calories"} render={() => <Calories />} />
        <Route exact path={"/search"} render={() => <SearchBar />} />
        <Route exact path={"/data"} render={() => <FakeUsers />} />
        <Route exact path ={"/foodresults"} render={() => <FoodResults />} />

        </React.Fragment>
    );
  }
}

export default App; 