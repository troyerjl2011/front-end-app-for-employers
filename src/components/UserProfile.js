import React, { Component } from "react";
import Charts from "./Charts";
import { Link } from "react-router-dom";
import WorkoutDropdown from "./WorkoutDropdown";
import { Button } from "shards-react";
import { connect } from "react-redux";
import { selectWorkout } from "../actions";
import "shards-ui/dist/css/shards.min.css";


const userInfo = {
  Name: "Billy",
  Age: 24,
  Height: 67,
  Weight: 145
};

class UserProfile extends Component {
  state = {
    selectedWorkout: ""
  };

  switchWorkouts = workoutName => {
    this.props.selectWorkout(workoutName);
  };
  state = { showDummyCharts: false };
  render() {
    return (
      <div className="userProfile">
        <div className="userinfo">
          <h1>User Info</h1>
          <h5>Name: {userInfo.Name} </h5>
          <h5>Age: {userInfo.Age} </h5>
          <h5>Height: {userInfo.Height} ins </h5>
          <h5>Weight: {userInfo.Weight} lbs </h5>
        </div>
        <Button
          onClick={() => this.setState({ showDummyCharts: true })}
          className="fullWeekButton"
          size="sm"
        >
          7 days worth of Workout
        </Button>

          <div className="selectedWorkout">
          <p>
            {this.props.selectedWorkout
              ? `Your workout: ${this.props.selectedWorkout}`
              : "You haven't selected a workout for today."}
          </p>
          {!this.props.selectedWorkout && (
            <WorkoutDropdown
              workoutNames={this.props.workouts}
              handleSwitchWorkout={this.switchWorkouts}
            />
          )}
          {this.props.selectedWorkout && (
            <Link to="/workout">
              <Button className="selectedWorkoutButton" size="sm">Checkout your workout</Button>
            </Link>
          )}
          </div>
        


        {this.state.showDummyCharts ? (
          <Charts />
        ) : (
          <h2>Enter your data for your fitness status</h2>
        )}

        
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    selectedWorkout: state.workouts.selectedWorkout,
    workouts: Object.keys(state.workouts.workouts)
  };
};
const mapDispatchToProps = {
  selectWorkout
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserProfile);
