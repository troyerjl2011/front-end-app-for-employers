import todoList from "./todos.json";
import workoutList2 from "./workout2.json";
import workoutList3 from "./crossfitWorkout.json";
import workoutList4 from "./scaledCrossfitWorkout.json";

const initialState = {
    "workouts": {
    "Power Lifting Day 1": todoList,
    "Power Lifting Day 2": workoutList2,
    "Crossfit Full Workout": workoutList3,
    "Scaled Crossfit Workout": workoutList4
    },
    selectedWorkout: ""
};

export default (state = initialState, action) => {
  switch (action.type) {
    case "SELECT_WORKOUT": 
      return {...state, selectedWorkout: action.payload}
    case "COMPLETED_EXERCISE":
      const newWorkouts = {...state.workouts}
      const workoutTodos = [...newWorkouts[state.selectedWorkout]]
      workoutTodos.forEach(todo => {
        if(todo.id === action.payload) {
          todo.completed = !todo.completed
        }
      })
      newWorkouts[state.selectedWorkout] = workoutTodos
      return { ...state, workouts: newWorkouts}
    case "RESET_EXERCISE_PAGE":
      const resetWorkouts = {...initialState.workouts}
      return { ...state, workouts: resetWorkouts, selectedWorkout: "" }

    default:
      return state;
  }
};


